#pragma once
#ifndef PROMOTE_H
#define PROMOTE_H

#include "IfThenElse.h"

//	template struct that Calculates and returns the larger of two
//	types
template <typename T1, typename T2>
struct Promotion
{
	using ResultT = typename IfThenElse<(sizeof(T1) > sizeof(T2)),
	T1, typename IfThenElse < (sizeof(T1) < sizeof(T2)),
		T2, void> ::ResultT
	>::ResultT;
};

//	partial spectialization can be used to handle identical inputs
template <typename T>
struct Promotion<T, T>
{
	using ResultT = T;
};

//	Because the sizes of some fundamental types will be equal we
//	need to introduce specializations to handle those cases.  This
//	can be simplified by defining a macro
//
#define MK_PROMOTION(T1, T2, Tr)	\
template <>							\
struct Promotion<T1, T2>			\
{									\
	using ResultT = Tr;				\
};									\
									\
template <>							\
struct Promotion<T2, T1>			\
{									\
	using ResultT = Tr;				\
};									\


//	Correct promotions can then be performed as follows
MK_PROMOTION(bool, char, int)
MK_PROMOTION(bool, unsigned char, int)
MK_PROMOTION(bool, signed char, int)

#endif