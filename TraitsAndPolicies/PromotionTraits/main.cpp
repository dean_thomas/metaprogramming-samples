#include "IfThenElse.h"
#include "Promote.h"
#include <vector>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <typeinfo>

using namespace std;

//	Add's two vectors of numbers, returning the appropriate type for the larger
//	input (avoiding any need for a narrowing of the output).
template <typename T1, typename T2>
vector<typename Promotion<T1, T2>::ResultT>
operator+ (vector<T1> const& a, vector<T2> const& b)
{
	assert(a.size() == b.size());

	//	User our Promotion<> meta-class to compute the required
	//	result type
	using ResultT = vector<typename Promotion<T1, T2>::ResultT>;
	
	//	Do the addition
	ResultT result(a.size());
	for (auto i = 0; i < a.size(); ++i)
	{
		result[i] = a[i] + b[i];
	}
	return result;
}

int main()
{
	//	c should be a vector<long long>
	vector<short> a = { 3, 19, 24, 50, 68 };
	vector<long long> b = { 97, 81, 76, 50, 32 };
	auto c = a + b;

	for (auto it = c.cbegin(); it != c.cend(); ++it)
	{
		cout << *it << " ";
	}

	cout << endl << typeid(c).name() << endl;

	//	z should be a vector<long double>
	vector<float> x = { 3.12f, 4.66f, 8.33f, 9.1f };
	vector<long double> y = { 6.88, 5.34, 1.67, 0.9 };
	auto z = x + y;

	for (auto it = z.cbegin(); it != z.cend(); ++it)
	{
		cout << *it << " ";
	}

	cout << endl << typeid(z).name() << endl;

	
	getchar();
	return 0;
}
