#pragma once
#ifndef TYPE_OP_H
#define TYPE_OP_H

//	Primary template
template <typename T>
struct TypeOp
{
	using ArgT = T;
	using BareT = T;
	using ConstT = T const;
	using RefT = T&;
	using RefBareT = T&;
	using RefConstT = T const &;
};

//	Partial specialization for const types
template <typename T>
struct TypeOp<T const>
{
	using ArgT = T const;
	using BareT = T;
	using ConstT = T const;
	using RefT = T const &;
	using RefBareT = T&;
	using RefConstT = T const &;
};

//	Partial specialization for references
template <typename T>
struct TypeOp<T&>
{
	using ArgT = T&;
	using BareT = typename TypeOp<T>::BareT;
	using ConstT = T const;
	using RefT = T&;
	using RefBareT = typename TypeOp<T>::BareT;
	using RefConstT = T const &;
};

//	*Full* specialization for void
template <>
struct TypeOp<void>
{
	using ArgT = void;
	using BareT = void;
	using ConstT = void const;
	using RefT = void;
	using RefBareT = void;
	using RefConstT = void;
};

#endif