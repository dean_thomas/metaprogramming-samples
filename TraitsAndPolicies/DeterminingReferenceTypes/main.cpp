//	Example of a a template that can deduce if a variable is a
//	constant and/or reference type
//
//	Taken from "D. Vandevoorde and N. M. Josuttis, �C++ Templates: The 
//	Complete Guide, 2002.� Addison-Wesley Publishing Company." 
//	(ch.15.2 [p.268-271])
//
#include <iostream>
#include "TypeOp.h"

using namespace std;

//	Applies the function (2nd parameter) to the first variable
template <typename T>
void apply(typename TypeOp<T>::RefT arg, void(*func)(T))
{
	func(arg);
}

//	Increment the referenced variable a
void incr(int& a)
{
	++a;
}

//	Print a copy of variable a
void print(int a)
{
	cout << a << endl;
}

int main()
{
	//	Uncomment this to illustrate compile time error
//#define ENABLE_ERRORS

	int x = 7;
	apply(x, print);
	apply(x, incr);
	apply(x, print);

#ifdef ENABLE_ERRORS
	//	These uses of 'apply' will be highlighted as
	//	syntax errors as it has been declared constant
	int const y = 10;
	apply(y, print);
	apply(y, incr);
	apply(y, print);

	//	These uses of 'apply' will be highlighted as
	//	syntax errors as it has been declared constant
	const int z = 10;
	apply(z, print);
	apply(z, incr);
	apply(z, print);
#endif

	//	This is allowable however, as w acts as an
	//	alias of x
	int& w = x;
	apply(w, print);
	apply(w, incr);
	apply(w, print);

	//	Make sure that we did actually increment x
	apply(x, print);

	getchar();
	return 0;
}