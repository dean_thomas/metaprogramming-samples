//	Tutorial as taken from:
//	http://www.codeproject.com/Articles/19989/Template-MetaProgramming-and-Number-Theory
//	[accessed: 24-11-2015]
//	and
//	http://www.codeproject.com/Articles/20180/Template-MetaProgramming-and-Number-Theory
//	[accessed: 24-11-2015]
//
//	Wiki on table of divisors:
//	https://en.wikipedia.org/wiki/Table_of_divisors
//	[accessed: 24-11-2015]
//
#include <iostream>
#include "NumberTheoryOps.h"
#include "StaticFuncs.h"

using namespace std;

void testPart1()
{
	using namespace NumberTheory1;

	const int n0 = 15;
	const int n1 = 8;

	auto gcd = Gcd<n0, n1>::value;
	auto lcm = Lcm<n0, n1>::value;
	auto coprime = CoPrime<n0, n1>::value;

	cout << "Greatest common denominator of " << n0
		<< " and " << n1 << " is: " << gcd << endl;
	cout << "Least common multiple of " << n0
		<< " and " << n1 << " is: " << lcm << endl;
	cout << "Is co-prime: " << ((coprime == 1) ? "true" : "false") << endl;

	const int n2 = 40;
	auto isPrime = IsPrime<n2>::value;
	auto numberOfDivisors = NumberOfDivisors<n2>::value;
	auto sumOfDivisors = SumOfDivisors<n2>::value;
	auto sumOfProperDivisors = SumOfProperDivisors<n2>::value;
	auto divisors2 = Divisor<n2, 2>::value;
	auto divisors3 = Divisor<n2, 3>::value;
	auto piFunc = PiFunc<n2>::value;

	cout << "The number " << n2 << " is prime: " << ((isPrime == 1) ? "true" : "false") << endl;
	cout << "Num of divisors: " << numberOfDivisors << endl;
	cout << "Sum of divisors: " << sumOfDivisors << endl;
	cout << "Sum of proper divisors: " << sumOfProperDivisors << endl;
	cout << "Sum of squares of divisors: " << divisors2 << endl;
	cout << "Sum of cubes of divisors: " << divisors3 << endl;
	cout << "Pi func: " << piFunc << endl;

	const int n3 = 42;
	auto isPrimeN3 = IsPrime<n3>::value;
	auto numberOfDivisorsN3 = NumberOfDivisors<n3>::value;
	auto sumOfDivisorsN3 = SumOfDivisors<n3>::value;
	auto sumOfProperDivisorsN3 = SumOfProperDivisors<n3>::value;

	cout << "The number " << n3 << " is prime: " << ((isPrime == 1) ? "true" : "false") << endl;
	cout << "Num of divisors: " << numberOfDivisorsN3 << endl;
	cout << "Sum of divisors: " << sumOfDivisorsN3 << endl;
	cout << "Sum of proper divisors: " << sumOfProperDivisorsN3 << endl;

	auto t = gcd * lcm;
	cout << ((t == n0 * n1) ? "True" : "False") << endl;
}

void testPart2()
{
	using namespace NumberTheory2;

	//	First test the loop function
	//	loop from -5..7, stepping +2, multiplying terms
	cout << ForLoop<-5, 7, 2, Add, Multiply, Value>::value << endl;

	//	loop from 16..1, stepping -3, adding terms
	cout << ForLoop<16, 1, 3, Subtract, Add, Value>::value << endl;

	//	Do some number thoery stuff...
	const auto n0 = 40;
	auto nd0 = NumberOfDivisors<n0>::value;
	auto sd0 = SumOfDivisors<n0>::value;

	cout << "Number of divisors for " << n0 << " is " << nd0 << endl;
	cout << "Number of divisors for " << n0 << " is " << sd0 << endl;
}

int main()
{
	testPart1();

	testPart2();

	


	getchar();
	return 0;
}