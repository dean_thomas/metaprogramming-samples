#pragma once
#ifndef STATICS_FUNCS_H
#define STATICS_FUNCS_H

#include "NumberTheoryOps.h"

namespace NumberTheory2
{
	template <int u, int v>
	struct Value
	{
		enum
		{
			value = u
		};
	};

	template <int u, int v = 0>
	struct Add
	{
		enum
		{
			value = u + v
		};
	};

	template <int u, int v = 0>
	struct Subtract
	{
		enum
		{
			value = u - v
		};
	};

	template <int u, int v = 1>
	struct Multiply
	{
		enum
		{
			value = u * v
		};
	};

	template <int u, int v = 1>
	struct Divide
	{
		enum
		{
			value = u / v
		};
	};

	template <int u, int v>
	struct Mod
	{
		enum
		{
			value = u % v
		};
	};

	//	Looping func
	//	Takes a start and end value, step size, step operator, and accumulation operator
	template <int Start, int End, int Exp = 1,
		template <int u, int v> class ExpOperator = Add,
		template <int u, int v> class Operator = Add,
		template<int u, int v> class BiFun = Value>
	struct ForLoop
	{
		enum
		{
			value =
			Operator<BiFun<Start, End>::value,
			ForLoop<ExpOperator<Start, Exp>::value, End, Exp,
			ExpOperator, Operator, BiFun>::value>::value
		};
	};

	//	Loop termination
	template <int End, int Exp,
		template <int u, int v> class ExpOperator,
		template <int u, int v> class Operator,
		template<int u, int v> class BiFun>
	struct ForLoop<End, End, Exp, ExpOperator, Operator, BiFun>
	{
		enum
		{
			value = BiFun<End, End>::value
		};
	};

	//	Calculate the number of divisors
	template <int n>
	struct NumberOfDivisors
	{
		enum { value = ForLoop<1, n, 1, Add, Add, NumberTheory1::Divisible>::value };
	};

	template <int n>
	struct SumOfDivisors
	{
		enum
		{
			value = ForLoop<1, n, 1, Add, Add, NumberTheory1::DivisibleDigit>::value
		};
	};

}

#endif