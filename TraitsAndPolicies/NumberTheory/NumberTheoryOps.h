#pragma once
#ifndef NUMBER_THEORY_OPS_H
#define NUMBER_THEORY_OPS_H

namespace NumberTheory1
{
	//	Can the number be divided exactly by an integer?
	template <int u, int v>
	struct Divisible
	{
		enum
		{
			value = u % v == 0 ? 1 : 0
		};
	};

	//	Partial specialization to avoid divide-by-zero
	template <int u>
	struct Divisible<u, 0>
	{
		enum
		{
			value = -1
		};
	};

	//	Calculate the ceiling value
	template <typename T>
	struct Ceil
	{
		static inline int value(T a)
		{
			return (a > 0 ? a + 1 : a);
		}
	};

	//	Calculate the floor value
	template <typename T>
	struct Floor
	{
		static inline int value(T a)
		{
			return (a > 0 ? a : a - 1);
		}
	};

	//	Is it an even number?
	template <int u>
	struct IsEven
	{
		enum
		{
			//	Call the divisible TMP function
			value = Divisible<u, 2>::value
		};
	};

	//	Is it an odd number?
	template <int u>
	struct IsOdd
	{
		enum
		{
			//	Call the divisible TMP function
			value = Divisible<u, 2>::value == 0 ? 1 : 0
		};
	};

	//	Greatest common denominator
	template <int u, int v>
	struct Gcd
	{
		enum
		{
			value = Gcd<v, u % v>::value
		};
	};

	//	Partially specialization for 0
	template <int u>
	struct Gcd<u, 0>
	{
		enum
		{
			value = u
		};
	};

	//	Full specialization for 0
	template <>
	struct Gcd<0, 0>
	{
		enum
		{
			value = -1
		};
	};

	//	Least common multiple
	template <int u, int v>
	struct Lcm
	{
		enum
		{
			value = u * v / Gcd<u, v>::value
		};
	};

	//	Check if numbers are co-prime (relative prime)
	template <int u, int v>
	struct CoPrime
	{
		enum
		{
			value = Gcd<u, v>::value == 1 ? 1 : 0
		};
	};

	//	Check if a number is prime
	template <int n>
	struct IsPrime
	{
		enum
		{
			value = NumberOfDivisors<n>::value == 2 ? 1 : 0
		};
	};



	//	Loop for total number of divisors
	template <int start, int end>
	struct NumberOfDivisorsLoop
	{
		enum
		{
			value = Divisible<end, start>::value + NumberOfDivisorsLoop<start + 1, end>::value
		};
	};

	//	Partial specilaization for loop termination
	template <int end>
	struct NumberOfDivisorsLoop<end, end>
	{
		enum
		{
			value = 1
		};
	};

	//	Get the number of divisors for any integer
	//	also known as [?_0(n)]
	template <int n>
	struct NumberOfDivisors
	{
		enum
		{
			value = NumberOfDivisorsLoop<1, n>::value
		};
	};




	//	Can the number be divided exactly by an integer?
	//	If so, we return the integer, else 0
	template <int u, int v>
	struct DivisibleDigit
	{
		enum
		{
			value = u % v == 0 ? v : 0
		};
	};

	//	Partial specialization to avoid divide-by-zero
	template <int u>
	struct DivisibleDigit<u, 0>
	{
		enum
		{
			value = -1
		};
	};

	//	Loop for sum of divisors
	template <int start, int end>
	struct SumOfDivisorsLoop
	{
		enum
		{
			value = DivisibleDigit<end, start>::value + SumOfDivisorsLoop<start + 1, end>::value
		};
	};

	//	Loop termination
	template <int end>
	struct SumOfDivisorsLoop<end, end>
	{
		enum
		{
			value = DivisibleDigit<end, end>::value
		};
	};

	//	Calculate the sum of the divisors
	//	Also known as the sigma function [?_1(n)]
	template <int n>
	struct SumOfDivisors
	{
		enum
		{
			value = SumOfDivisorsLoop<1, n>::value
		};
	};

	//	Calculate the sum of the proper divisors
	//	(the sum of divisions, upto but not including
	//	the number itself).
	//	Also known as the: aliquot sum [s(n)]
	template <int n>
	struct SumOfProperDivisors
	{
		enum
		{
			value = SumOfDivisorsLoop<1, n>::value - n
		};
	};



	//	To calculate the power
	template <int a, int b>
	struct Power
	{
		enum
		{
			value = a * Power<a, b - 1>::value
		};
	};

	//	Partial specialization for a^0
	template <int a>
	struct Power<a, 0>
	{
		enum
		{
			value = 1
		};
	};

	//	Divisors function loop
	template <int start, int end, int x>
	struct DivisorLoop
	{
		enum
		{
			value = (Divisible<end, start>::value == 1 ? Power<start, x>::value : 0) + DivisorLoop<start + 1, end, x>::value
		};
	};
#pragma endregion

	//	Loop termination
	template <int end, int x>
	struct DivisorLoop<end, end, x>
	{
		enum
		{
			value = Power<end, x>::value
		};
	};

	//	Calculate the divisor function [?_x(n)]
	//	https://en.wikipedia.org/wiki/Divisor_function [25-11-2015]
	//
	//	Note that if:
	//		x == 0 : the series is the number-of-divisors function
	//		x == 1 : the series is the sum-of-divisors function
	//		x == 2 : the series is the sum-of-the-squares-of-divisors function
	//		and so on...
	//
	template <int n, int x>
	struct Divisor
	{
		enum
		{
			value = DivisorLoop<1, n, x>::value
		};
	};




	//	pi function: counts the number of primes less that or equal to n
	//	https://en.wikipedia.org/wiki/Prime-counting_function [25-11-2015]
	template <int n>
	struct PiFunc
	{
		enum
		{
			value = IsPrime<n>::value + PiFunc<n - 1>::value
		};
	};

	//	Full specialization for when n=2
	template <>
	struct PiFunc<2>
	{
		enum
		{
			value = 1
		};
	};




	//	Loop for calculating the totient function
	template <int start, int end>
	struct TotientLoop
	{
		enum
		{
			value = CoPrime<start, end>::value + TotientLoop<start + 1, end>::value
		};
	};

	//	Loop termination
	template <int end>
	struct TotientLoop<end, end>
	{
		enum
		{
			value = 0
		};
	};

	//	Totient function
	template <int n>
	struct Totient
	{
		enum
		{
			value = TotientLoop<1, n>::value
		};
	};

	//	Full specialization for n = 1
	template <>
	struct Totient<1>
	{
		enum
		{
			value = 1
		};
	};

	//	Full specialization for n = 0
	template <>
	struct Totient<0>
	{
		enum
		{
			value = 1
		};
	};
}

#endif