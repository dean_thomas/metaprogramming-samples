#ifndef MULTIPLY_POLICY_H
#define MULTIPLY_POLICY_H

//	Define our two input/output types
template <typename T1, typename T2>
struct MultiplyPolicy
{
	//	Define our operator - note: we return the value in total
	static void accumulate(T1& total, T2 const& value)
	{
		total *= value;
	}
};

#endif