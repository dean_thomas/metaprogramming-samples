#ifndef ACCUMULATOR_H
#define ACCUMULATOR_H

#include "AccumulatorTraits.h"
#include "SumPolicy.h"
#include "MultiplyPolicy.h"

//	T:		defines the type being accumulated
//	Policy: defines a default policy as a template class
//	Traits: defines the traits of type T
template
<
	typename T,
	template<typename, typename> class Policy = SumPolicy,
	typename Traits = AccumulatorTraits < T >
>
inline typename AccumulatorTraits<T>::AccT Accumulator(T const* beg, T const* end)
{
	//	Retreive the return type from the AccumulatorTraits
	//	class.
	using AccT = typename Traits::AccT;

	//	Initialize our total to zero, using the definiton
	//	in the AccumulatorTraits class.
	AccT total = Traits::zero();

	//	Sum over the pointer range.
	while (beg != end)
	{
		Policy<AccT, T>::accumulate(total, *beg);
		++beg;
	}

	//	And return the result.
	return total;
}

#endif