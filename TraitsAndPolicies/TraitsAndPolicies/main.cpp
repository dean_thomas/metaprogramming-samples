//	Example of a configurable template method using policies and Traits.
//
//	Taken from "D. Vandevoorde and N. M. Josuttis, �C++ Templates: The 
//	Complete Guide, 2002.� Addison-Wesley Publishing Company." 
//	(ch.15.1 [p.245-263])
#include "Accumulator.h"
#include <cstdlib>
#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int input[] = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 };

	//	Do an accumulation using the default '+' operator
	cout << Accumulator<int>(&input[0], &input[10]) << endl;

	//	Do an accumulation, overriding the operator to '*'
	cout << Accumulator<int, MultiplyPolicy>(&input[0], &input[10]) << endl;

	getchar();
	return 0;
}