#ifndef ACCUMULATOR_TRAITS_H
#define ACCUMULATOR_TRAITS_H

//	Forward declaration of our template class
template <typename T>
struct AccumulatorTraits;

//	Explicit specialization for T = char
template <>
struct AccumulatorTraits<char>
{
	//	Return type will be an int
	using AccT = int;
	
	//	We define our definition of zero as a
	//	static function.  This allows us to get
	//	around C++'s limitation of only being able
	//	to initialize static INTEGRAL data types.
	//	This allows us to use the non-integral
	//	data type 'double' as a return type.  
	//	However, this can also be extended to our 
	//	own defined classes.
	static AccT zero() { return 0; }
};

//	Explicit specialization for T = short
template <>
struct AccumulatorTraits<short>
{
	//	Return type will be an int
	using AccT = int;

	//	We define our definition of zero as a
	//	static function.  This allows us to get
	//	around C++'s limitation of only being able
	//	to initialize static INTEGRAL data types.
	//	This allows us to use the non-integral
	//	data type 'double' as a return type.  
	//	However, this can also be extended to our 
	//	own defined classes.
	static AccT zero() { return 0; }
};

//	Explicit specialization for T = int
template <>
struct AccumulatorTraits<int>
{
	//	Return type will be a long int
	using AccT = long;

	//	We define our definition of zero as a
	//	static function.  This allows us to get
	//	around C++'s limitation of only being able
	//	to initialize static INTEGRAL data types.
	//	This allows us to use the non-integral
	//	data type 'double' as a return type.  
	//	However, this can also be extended to our 
	//	own defined classes.
	static AccT zero() { return 0; }
};

//	Explicit specialization for T = unsigned int
template <>
struct AccumulatorTraits<unsigned int>
{
	//	Return type will be an unsigned long int
	using AccT = unsigned long;

	//	We define our definition of zero as a
	//	static function.  This allows us to get
	//	around C++'s limitation of only being able
	//	to initialize static INTEGRAL data types.
	//	This allows us to use the non-integral
	//	data type 'double' as a return type.  
	//	However, this can also be extended to our 
	//	own defined classes.
	static AccT zero() { return 0; }
};

//	Explicit specialization for T = float
template <>
struct AccumulatorTraits<float>
{
	//	Return type will be an double
	using AccT = double;

	//	We define our definition of zero as a
	//	static function.  This allows us to get
	//	around C++'s limitation of only being able
	//	to initialize static INTEGRAL data types.
	//	This allows us to use the non-integral
	//	data type 'double' as a return type.  
	//	However, this can also be extended to our 
	//	own defined classes.
	static AccT zero() { return 0; }
};


#endif
