#ifndef ELEMENT_TYPE_H
#define ELEMENT_TYPE_H

#include <vector>
#include <list>
#include <stack>
#include <iostream>
#include <typeinfo>

//	Primary template declaration
template <typename T>
struct ElementType;

//	Default implementation: relies upon the 
//	container 'C' providing a 'value_type' 
//	field.  Specializations will override
//	this.
template <typename C>
struct ElementType
{
	//	Retreive Type from container 'C'
	using Type = typename C::value_type;
};

//	Partial specialization for std::vector
template <typename T>
struct ElementType<std::vector<T>>
{
	using Type = T;
};

//	Partial specialization for std::list
template <typename T>
struct ElementType<std::list<T>>
{
	using Type = T;
};

//	Partial specialization for std::stack
template <typename T>
struct ElementType<std::stack<T>>
{
	using Type = T;
};

//	Our generic print function
template <typename T>
void print_element_type(T const& c)
{
	std::cout << "Container of '"
		//	Retreive the name of the element type using
		//	the ElementType template structs.
		<< typeid(typename ElementType<T>::Type).name()
		<< "' elements." << std::endl;
}



#endif