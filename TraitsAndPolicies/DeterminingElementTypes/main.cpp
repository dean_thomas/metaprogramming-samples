//	Example of a configurable template method using policies and Traits.
//
//	Taken from "D. Vandevoorde and N. M. Josuttis, �C++ Templates: The 
//	Complete Guide, 2002.� Addison-Wesley Publishing Company." 
//	(ch.15.2 [p.263-265])
#include <vector>
#include <stack>
#include <array>
#include <list>
#include <string>
#include "ElementType.h"

using namespace std;

int main()
{
	vector<int> myVec = { 1, 2 , 3, 4, 5 };
	stack<string> myStack;
	myStack.push("ABC");
	array<float*,2> myArr = { new float(3.3f), new float(3.8f) };
	string myString = "Hello world!";

	print_element_type(myVec);
	print_element_type(myStack);
	print_element_type(myArr);
	print_element_type(myString);

	getchar();
	return 0;
};