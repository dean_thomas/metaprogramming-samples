#pragma once
#ifndef RPARAM_CLASS_H
#define RPARAM_CLASS_H

#include <iostream>
#include "RParam.h"

using namespace std;

struct MyStruct1
{
	MyStruct1()
	{ }

	MyStruct1(MyStruct1 const&)
	{
		cout << "MyStruct1 copy constructor called" << endl;
	}
};

struct MyStruct2
{
	MyStruct2()
	{ }

	MyStruct2(MyStruct2 const&)
	{
		cout << "MyStruct2 copy constructor called" << endl;
	}
};

//	This specialization will enforce that myStruct2 is
//	always called by Value, not by reference
template <>
struct RParam<MyStruct2>
{
	using Type = MyStruct2;
};

#endif