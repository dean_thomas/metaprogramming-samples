#pragma once
#ifndef RPARAM_H
#define RPARAM_H

//	Re-used from "Promotion traits" project
#include "IfThenElse.h"
// Re-used from "DeterminingClassTypes" project
#include "IsClassType.h"

template <typename T>
struct RParam
{
	using Type = typename IfThenElse<IsClassTypeImpl<T>::No, T, T const&>::ResultT;
};


#endif