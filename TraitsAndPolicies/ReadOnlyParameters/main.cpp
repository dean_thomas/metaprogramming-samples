#include <iostream>
#include <cstdio>
#include "RParam.h"
#include "RParamClass.h"

using namespace std;

#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

//	Create a function that can be called with template parameters.
//	Wether a type is passed by reference or value is determined by the
//	RParam meta-function (or a template specialization).  This function
//	needs to be qualified with the inner template parameters.
template <typename T1, typename T2>
void foo(typename RParam<T1>::Type p1, typename RParam<T2>::Type p2)
{
	cout << "Function '"<< __PRETTY_FUNCTION__ << "' was called." << endl;
}

//	Create a function that can be called with template parameters.
//	Wether a type is passed by reference or value is determined by the
//	RParam meta-function (or a template specialization).  This function
//	also needs to be qualified with the inner template parameters.
template <typename T1, typename T2>
void barImplementation(typename RParam<T1>::Type p1, typename RParam<T2>::Type p2)
{
	cout << "Function '" << __PRETTY_FUNCTION__ << "' was called." << endl;
}

//	We can create a wrapper for the function, so that template
//	parameters don't need to be stated in the function call.
template <typename T1, typename T2>
inline void bar(T1 const& p1, T2 const& p2)
{
	barImplementation<T1, T2>(p1, p2);
}

int main()
{
	MyStruct1 ms1;
	MyStruct2 ms2;

	int simpleType = 1;

	//	ms1 will be passed by reference, ms2 by value (as it has been
	//	explicilty asked to in RParamClass). 
	foo<MyStruct1, MyStruct2>(ms1, ms2);
	
	//	This cannot be called without explicitly stating the
	//	template parameters in its default form
	//	foo(ms1, ms2);

	//	Simple 'imperative' types are called by value
	foo<MyStruct1, int>(ms1, simpleType);

	//	Create a new struct
	struct MyNewStruct
	{ } myNewStruct;

	//	Because we have not asked to have MyNewStruct passed
	//	by value, it is passed as a reference by default
	foo<MyNewStruct, MyStruct1>(myNewStruct, ms1);

	//	Does the same, except now we use our wrapped function
	//	calls.
	bar(ms1, ms2);
	bar(ms1, myNewStruct);
	bar(simpleType, myNewStruct);

	getchar();
	return 0;
}