//	Example of a function that is able to determine if an object is of
//	class type or not (true for classes, structs and unions).
//
//	Taken from "D. Vandevoorde and N. M. Josuttis, �C++ Templates: The 
//	Complete Guide, 2002.� Addison-Wesley Publishing Company." 
//	(ch.15.2 [p.266-268])
//
#include <iostream>
#include <vector>
#include <string>

#include "IsClassType.h"

using namespace std;

//	Define some object types for our tests (note: some don't need a
//	definition as they are never instantiated).
class MyClass;
union MyUnion;

struct MyStruct
{ };

void myFunc() { }

enum MyEnum
{
	e1, e2
};

enum class MyEnumClass
{
	ec1, ec2
};

int main()
{
	//	true/false not 0/1 in output
	cout << boolalpha;

	//	Test by passing the type as a template parameter
	cout << "int is class type: " << IsClassType<int>() << endl;
	cout << "float is class type: " << IsClassType<float>() << endl;
	cout << "MyClass is class type: " << IsClassType<MyClass>() << endl;
	cout << "vector<int> is class type: " << IsClassType<vector<int>>() << endl;
	cout << "MyUnion is class type: " << IsClassType<MyUnion>() << endl;

	//	Instantiate some objects
	MyStruct myStruct;
	MyEnum myEnum = e1;
	MyEnumClass myEnumClass = MyEnumClass::ec1;

	//	Test by passing the type as an instantiated object
	cout << "MyStruct is class type: " << IsClassType(myStruct) << endl;
	cout << "MyEnum is class type: " << IsClassType(myEnum) << endl;
	cout << "MyEnumClass is class type: " << IsClassType(myEnumClass) << endl;
	cout << "myFunc() is class type: " << IsClassType(myFunc) << endl;

	getchar();
	return 0;
}