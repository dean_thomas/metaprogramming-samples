#pragma once
#ifndef IS_CLASS_TYPE_H
#define IS_CLASS_TYPE_H

///
///		\brief		Implementation that does the test, should be called by 
///					the 'bool IsClassType<T>()' and 'bool IsClassType(T)'
///					methods below.
///
///		\since		21-11-2015
///		\author		Dean:	Move this into our IsClassType header,
///					in the book example is in the main.cpp [21-11-2015]
///
///		\example	IsClassType<T>()
///
template <typename T>
class IsClassTypeImpl
{
private:
	using One = char;
	using Two = struct
	{
		char a[2];
	};

	//	pointer-to-member types (C::*) is only valid 
	//	when C is a class type
	template<typename C> 
	static One test(int C::*);

	template<typename C> 
	static Two test(...);

public:
	enum
	{
		//	Note: In order to compile with MSVC we need to remove 
		//	the 'IsClassType<T>::' qualifier below.  See:
		//	http://stackoverflow.com/questions/2454817/error-c2784-could-not-deduce-template-argument
		//	accessed [21-11-2015]
		Yes = sizeof(test<T>(0)) == 1
	};
	enum { No = !Yes };
};

///
///		\brief		Returns true if the type T is a class type
///
///		\since		21-11-2015
///		\author		Dean:	Move this into our IsClassType header,
///					in the book example is in the main.cpp [21-11-2015]
///
///		\example	IsClassType<T>()
///
template <typename T>
bool IsClassType()
{
	return (IsClassTypeImpl<T>::Yes);
}

///
///		\brief		Returns true if the type T is a class type
///
///		\since		21-11-2015
///		\author		Dean:	Move this into our IsClassType header,
///					in the book example is in the main.cpp [21-11-2015]
///
///		\example	IsClassType(T t)
///
template <typename T>
bool IsClassType(T)
{
	return IsClassType<T>();
}

#endif