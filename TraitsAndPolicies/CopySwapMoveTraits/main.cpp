#include "CSMTraits.h"
#include <iostream>
#include <cstdio>

using namespace std;

//	Simple class that will evaluate to needing non-bitwise copy
class MyNonPODClass
{
public:
	size_t m_id;

	MyNonPODClass(const size_t& id)
		: m_id{ id } { }
};

//	Because MyPODClass1 is an alias for a integral type, it will
//	evaluate as being safe for a bitwise copy
using MyPODClass1 = unsigned long long;

//	By default MyPODClass2 will not qualify as being suitable for
//	bit-wise copy
struct MyPODClass2
{
	size_t data;
};

//	However, we can make it act that way using a template specialization
template<>
struct CSMTraits<MyPODClass2> : public BitOrClassCSM<MyPODClass2, true> 
{ };

int main()
{
	//	Test 1 - should use a non-bitwise copy
	//	======================================
	MyNonPODClass a[] = { 255, 20, 12, 23 };
	MyNonPODClass b[] = { 122, 34, 21, 33 };

	for (auto i = 0; i < 4; ++i)
	{
		cout << a[i].m_id << " " << b[i].m_id << endl;
	}

	CSMTraits<MyNonPODClass>::copy_n(a, b, 4);
	for (auto i = 0; i < 4; ++i)
	{
		cout << a[i].m_id << " " << b[i].m_id << endl;
	}

	cout << endl;

	//	Test 2 - should use a bitwise copy by default
	//	=============================================
	MyPODClass1 x[] = { 33, 44, 55, 66 };
	MyPODClass1 y[] = { 31, 41, 51, 61 };

	for (auto i = 0; i < 4; ++i)
	{
		cout << x[i] << " " << y[i] << endl;
	}

	CSMTraits<MyPODClass1>::copy_n(x, y, 4);
	for (auto i = 0; i < 4; ++i)
	{
		cout << x[i] << " " << y[i] << endl;
	}

	cout << endl;

	//	Test 3 - should use a bitwise copy, because of the template
	//	specialization
	//	===========================================================
	MyPODClass2 x1[] = { 33, 44, 55, 66 };
	MyPODClass2 y1[] = { 31, 41, 51, 61 };

	for (auto i = 0; i < 4; ++i)
	{
		cout << x1[i].data << " " << y1[i].data << endl;
	}

	CSMTraits<MyPODClass2>::copy_n(x1, y1, 4);
	for (auto i = 0; i < 4; ++i)
	{
		cout << x1[i].data << " " << y1[i].data << endl;
	}

	getchar();
	return 0;
}