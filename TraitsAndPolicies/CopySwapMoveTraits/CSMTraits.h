#pragma once
#ifndef CSM_TRAITS_H
#define CSM_TRAITS_H

//	Re-used from 'DeterminingClassTypes' project
#include "IsClassType.h"
#include "CSM.h"

template <typename T>
class CSMTraits : public BitOrClassCSM<T, IsClassTypeImpl<T>::No>
{ };

#endif