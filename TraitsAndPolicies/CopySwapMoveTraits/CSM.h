#pragma once
#ifndef CSM_H
#define CSM_H

#include <new>
#include <cassert>
#include <cstddef>
#include <cstring>

//	Re-used from 'ReadOnlyParams' project
#include "RParam.h"

#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

//	Forward declaration
template <typename T, bool Bitwise>
struct BitOrClassCSM;

//	Partial specialization for safe copying of objects
template <typename T>
struct BitOrClassCSM<T, false>
{
	static void copy(typename RParam<T>::Type src, T* dst)
	{
		//	copy one item onto another
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		*dst = src;
	}

	static void copy_n(T const* src, T* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	copy n items onto n others
		for (size_t k = 0; k < n; ++k)
		{
			dst[k] = src[k];
		}
	}

	static void copy_init(typename RParam<T>::Type src, void* dst)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	copy n items onto uninitialized storage
		::new(dst) T(src);
	}

	static void copy_init_n(T const* src, void* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	copy n items onto uninitialized storage
		for (size_t k = 0; k < n; ++k)
		{
			::new((void*)((char*)dst + k)) T(src[k]);
		}
	}

	static void swap(T* a, T* b)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Swaps two items
		T tmp(*a);
		*a = *b;
		*b = tmp;
	}

	static void swap_n(T* a, T* b, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Swap n items
		for (size_t k = 0; k < n; ++k)
		{
			T tmp(a[k]);
			a[k] = b[k];
			b[k] = tmp;
		}
	}

	static void move(T* src, T* dst)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Move one item onto another
		assert(src != dst);

		*dst = *src;
		src->~T();
	}

	static void move_n(T* src, T* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Move n items on another
		assert(src != dst);
		for (size_t k = 0; k < n; ++k)
		{
			dst[k] = src[k];
			src[k] .~T();
		}
	}

	static void move_init(T* src, void* dst)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Move an item onto unitialized storage
		assert(src != dst);
		::new(dst) T(*src);
		src->~T();
	}

	static void move_init_n(T const* src, void* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Move n items onto unitialized storage
		assert(src != dst);
		for (size_t k = 0; k < n; ++k)
		{
			::new((void*)((char*)dst + k)) T(src[k]);
			src[k].~T();
		}
	}
};

template <typename T>
struct BitOrClassCSM<T, true> : public BitOrClassCSM<T, false>
{
	//	These methods inherited directly from partial specialization above
	//	static void copy(typename RParam<T>::ResultT src, T* dst);
	//	static void copy_init(typename RParam<T>::ResultT src, void* dst);
	//	static void swap(T* a, T* b);
	//	static void swap_n(T* a, T* b, size_t n);
	//	static void move(T* src, T* dst);
	//	static void move_init(T* src, void* dst);
	//
	static void copy_n(T const* src, T* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	copy n items onto n others
		std::memcpy((void*)dst, (void*)src, n * sizeof(T));
	}

	static void copy_init_n(T const* src, void* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	copy n items onto uninitialized storage
		std::memcopy(dst, (void*)src, n * sizeof(T));
	}

	static void move_n(T* src, T* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Move n items on another
		assert(src != dst);
		std::memcopy((void*)dst, (void*)src, n * sizeof(T));
	}

	static void move_init_n(T const* src, void* dst, size_t n)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		//	Move n items onto unitialized storage
		assert(src != dst);
		std::memcpy(dst, (void*)src, n * sizeof(T));
	}
};

#endif