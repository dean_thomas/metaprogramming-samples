#pragma once
#ifndef POW_3_H
#define POW_3_H

///	Compile time algorithm for computing powers of 3
///	
///	Taken from: C++ templates, the complete guide
///	Chapter 17.1 (pg. 302-303)
///	[26-12-2015]
///
template <size_t N>
struct Pow3
{
	enum { result = 3 * Pow3<N - 1>::result };
};

//	Full specifictaion for N = 0.  Ends recursion
template <>
struct Pow3<0>
{
	enum { result = 1 };
};

#endif