#include "Pow3.h"
#include <cstdio>
#include <iostream>


using namespace std;

int main()
{
	auto TAB = '\t';

	cout << "3^0" << TAB
		<< "3^1" << TAB
		<< "3^2" << TAB
		<< "3^3" << TAB
		<< "3^4" << TAB
		<< "3^5" << TAB
		<< endl;

	cout << Pow3<0>::result << TAB
		<< Pow3<1>::result << TAB
		<< Pow3<2>::result << TAB
		<< Pow3<3>::result << TAB
		<< Pow3<4>::result << TAB
		<< Pow3<5>::result << TAB
		<< endl;

	getchar();
	return 0;
}