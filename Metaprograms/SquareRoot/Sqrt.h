#pragma once
#ifndef SQRT_H
#define SQRT_H

#include "IfThenElse.h"

///	Recursive Sqrt computation algorithm using a binary search algorithm
///	
///	Taken from: C++ templates, the complete guide
///	Chapter 17.3 (pg. 305-309)
///	[26-12-2015]
///
template <size_t N, size_t LO=1, size_t HI=N>
struct Sqrt
{
	//	Compute midpoint (rounded up)
	enum { mid = (LO + HI + 1) / 2 };

	//	Naive implementation can result in a large amount of recursice templates
	//	being created (both paths of terniary operator will be evaluated)
	//	enum { result = (N < mid * mid) ? Sqrt<N, LO, mid-1>::result : Sqrt<N, mid, HI>::result };

	//	Optimised solution only creates templates for the required path
	using SubT = typename IfThenElse < (N < (size_t)(mid * mid)), Sqrt<N, LO, mid - 1>, Sqrt < N, mid, HI >> ::ResultT;

	enum { result = SubT::result };
};

//	Specialization for LO == HI
template <int N, int M>
struct Sqrt<N, M, M>
{
	enum { result = M };
};

#endif