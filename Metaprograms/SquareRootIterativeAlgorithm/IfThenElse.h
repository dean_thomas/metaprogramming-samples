#pragma once
#ifndef IF_THEN_ELSE_H
#define IF_THEN_ELSE_H

//	Forward declaration of functions defined below
template <bool C, typename Ta, typename Tb>
struct IfThenElse;

//	Partial specialization
//	True yields type Ta
template <typename Ta, typename Tb>
struct IfThenElse<true, Ta, Tb>
{
	using ResultT = Ta;
};

//	Partial specialization
//	False yields type Tb
template <typename Ta, typename Tb>
struct IfThenElse<false, Ta, Tb>
{
	using ResultT = Tb;
};


#endif