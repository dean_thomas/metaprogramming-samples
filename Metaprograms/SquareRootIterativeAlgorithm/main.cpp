#include "Sqrt.h"
#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
	auto TAB = '\t';

	cout << Sqrt<4>::result << TAB
		<< Sqrt<9>::result << TAB
		<< Sqrt<16>::result << TAB
		<< Sqrt<25>::result << TAB
		<< Sqrt<36>::result << TAB
		<< endl;

	getchar();
	return 0;
}