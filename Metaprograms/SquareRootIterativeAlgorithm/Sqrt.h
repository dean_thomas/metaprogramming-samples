#pragma once
#ifndef SQRT_H
#define SQRT_H

#include "IfThenElse.h"

///	Recursive Sqrt computation algorithm using a iterative search algorithm
///	
///	Taken from: C++ templates, the complete guide
///	Chapter 17.3 (pg. 310-312)
///	[26-12-2015]
///
template <size_t N, size_t I = 1>
struct Sqrt
{
	//	Naive solution will create all possible templates
	//	enum { result = (I * I < N) ? Sqrt<N, I + 1>::result : I };

	//	More efficient solution only creates templates for the
	//	required branches
	using SubT = typename IfThenElse < (I * I < N), Sqrt<N, I + 1>, Value<I>>::ResultT;

	//	Return the result from branching
	enum { result = SubT::result };
};

//	Partial specialization ends the iteration
template <size_t N>
struct Sqrt<N, N>
{
	enum { result = N };
};

//	Turns a template argument into a enum constant
//	used when the IfThenElse statement evaluates to false.
template <size_t N>
struct Value
{
	enum { result = N };
};


#endif